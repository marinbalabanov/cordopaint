class Painter {
    constructor( id ) {
        this.canvasid = id;
        this.ctx = null;
        this.paintColor = 'red';
        this.dotSize = 10;
        this.drawModus = 'line'; //'line'
        this.lastX = -1;
        this.lastY = -1;
        this.initCanvas();
    }

    initCanvas() {
        $( this.canvasid ).attr({
            width:$(window).width(),
            height:$(window).height()
        })/*.on( 'touchend', e => {
            this.lastX = -1;
            this.lastY = -1;
        })*/.on( 'touchstart', e => {

            if ( !this.bild ) { return false; }

            this.lastX = e.originalEvent.touches[0].clientX;
            this.lastY = e.originalEvent.touches[0].clientY; 
            this.drawDot( this.lastX, this.lastY );
        }).on( 'touchmove', e => {

            if ( !this.bild ) { return false; }

            this.draw(e.originalEvent.touches[0].clientX,e.originalEvent.touches[0].clientY);            
        });
        this.ctx = $( '#imagefield' ).get(0).getContext( '2d' );
        this.ctx.fillStyle =  this.ctx.strokeStyle =  this.paintColor;
    }

    drawDot(x,y) {
        this.ctx.beginPath();
        this.ctx.arc(x,y,this.dotSize/2,0,2*Math.PI);
        this.ctx.fill();
    }

    drawLine( x,y) {
        this.ctx.beginPath();
        this.ctx.moveTo(this.lastX,this.lastY);
        this.ctx.lineTo(x,y);
        this.ctx.stroke();
    }

    draw( x,y ) {
        this.ctx.fillStyle =  this.ctx.strokeStyle =  this.paintColor;
        this.ctx.lineWidth = this.dotSize;
        this.ctx.lineCap = 'round';
        this.ctx.Join = 'round'; 
        switch( this.drawModus ) {
            case 'dot':
                this.drawDot( x,y );
                break;
            case 'line':
                this.drawLine( x,y );      
                break;
        }
        this.lastX = x;
        this.lastY = y;        
    }

    clear() {          
        console.log( this ); // ??        
        this.ctx.clearRect(0,0,2000,2000);
        this.loadImage( this.bild );
    }

    loadImage( bild ) {        
        this.ctx.drawImage( bild, 0, 0, bild.width, bild.height, 0, 0, $(window).width(), $(window).height() );
        this.bild = bild;
    }

    save() {
        
        if ( APP ) {
            window.canvas2ImagePlugin.saveImageDataToLibrary(
                () => {
                    alert( 'Bild wurde gespeichert' );                
                },
                () => {
                    alert( 'Speichern nicht möglich.')
                },
                $( this.canvasid ).get(0)
            ).bind( this )
        } else {
            alert( 'Speichern nur in APP möglich');
        }
    }

}

var backcameraLoadImage = () => {
    if ( APP ) {
        navigator.camera.getPicture( 
          ( image ) => {
              var bild = new Image();
              bild.src = image;
              bild.onload = () => {
                  painter.loadImage( bild );
              }
          },
          () => {
              alert( 'Bild Error' );
          },
          {
              correctOrientation:true,
              cameraDirection: back
          }
  
        )
    } else {
        // lade ein testbild
        var bild = new Image();
        bild.src = 'testassests/mungu_jumba_011.jpg';
        bild.onload = () => {
          painter.loadImage( bild );
      }
  
    }
}

var frontcameraLoadImage = () => {
    if ( APP ) {
        navigator.camera.getPicture( 
          ( image ) => {
              var bild = new Image();
              bild.src = image;
              bild.onload = () => {
                  painter.loadImage( bild );
              }
          },
          () => {
              alert( 'Bild Error' );
          },
          {
              correctOrientation:true,
              cameraDirection: front
          }
  
        )
    } else {
        // lade ein testbild
        var bild = new Image();
        bild.src = 'testassests/mungu_jumba_012.jpg';
        bild.onload = () => {
          painter.loadImage( bild );
      }
  
    }
}

var painter;
var starteApp = () => {
  painter = new Painter('#imagefield');
  $( '<button>' ).html( 'Clear Image' ).appendTo( 'body' ).css({position:'absolute',top:0,left:0}).on( 'click', painter.clear.bind( painter ) );
  $( '<button>' ).html( 'Back Camera' ).appendTo( 'body' ).css({position:'absolute',top:0,right:0}).on( 'click', backcameraLoadImage );
  $( '<button>' ).html( 'Front Camera' ).appendTo( 'body' ).css({position:'absolute',bottom:0,right:0}).on( 'click', frontcameraLoadImage );
  $( '<button>' ).html( 'Save Image' ).appendTo( 'body' ).css({position:'absolute',bottom:0,left:0}).on( 'click', painter.save.bind(painter) );
}

if ( APP ) {
    document.addEventListener( 'deviceready', starteApp );
} else {
    // zum Testen ohne PhoneGap
    starteApp();
}



